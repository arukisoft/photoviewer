//
//  arukiAppDelegate.h
//  PhotoViewerProject
//
//  Created by takashiikura on 2014/05/17.
//  Copyright (c) 2014年 takashiikura. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface arukiAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
